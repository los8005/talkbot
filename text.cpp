#include "text.h"

bool ToLowerRusCase(char Target[MAX_TEXT],char RetBuffer[MAX_TEXT])
{
	wchar_t buffer[MAX_TEXT];
	if(mbstowcs(buffer, Target, MAX_TEXT) == -1)
		return false;
	for(int i = 0;i<wcslen(buffer);i++)
		buffer[i] = towlower(buffer[i]);
	if(wcstombs(RetBuffer, buffer, MAX_TEXT) == -1)
		return false;
	return true;
}

int ParseText(char SourceText[MAX_TEXT],char Result[MAX_TEXT])
{
	if(setlocale(LC_ALL, "") == NULL)
		return -1;

	int count = 0;
	struct sb_stemmer *stemmer = sb_stemmer_new(LANG_TO_PARSE, CODEPAGE);
	if(stemmer == NULL)
		return -1;
	
	char LowerSource[MAX_TEXT] = {0};

	if(ToLowerRusCase(SourceText,LowerSource) == false)
	{
		sb_stemmer_delete(stemmer);
		return -1;
	}

	char *word = strtok(LowerSource,SEPARATOR);
	
	if(word == NULL)
	{
		sb_stemmer_delete(stemmer);
		return -1;
	}

	do
	{
		char *resword = (char *)sb_stemmer_stem(stemmer, (unsigned char *)word, strlen(word));
		if(resword == NULL)
		{
			sb_stemmer_delete(stemmer);
			return -1;
		}

		strcpy(Result,resword);
		Result += (strlen(Result) + 1);
		count++;
	}while((word = strtok(NULL,SEPARATOR)) != NULL);

	sb_stemmer_delete(stemmer);
	return count;
}