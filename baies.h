#ifndef BAIES
#define BAIES

#include "headers.h"
#include "text.h"
#include "dataio.h"

#define DOCS_PER_CLASS "DocsPerClass" // у каждого класса
#define WORD_PER_CLASS "WordPerClass" // у каждого класса
#define DOCS_PER_TOTAL "DocsPerTotal" // у всей базы
#define UNIQ_WORD_PER_TOTAL "UniqWordPerTotal" // у всей базы

bool Train(char ClassName[MAX_LINE],char TrainData[MAX_TEXT]);
bool AnalyzeReplica(char AnalyzeData[MAX_TEXT],char ClassName[MAX_LINE]);

#endif