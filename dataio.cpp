#include "dataio.h"

struct Item *CreateItem(struct Item *head,char CategoryName[MAX_LINE],char ItemName[MAX_LINE],char ItemValue[MAX_LINE])
{
	struct Item *item = (struct Item *)malloc(sizeof(struct Item));
	if(!item)
		return NULL;
	strcpy(item->CategoryName,CategoryName);
	strcpy(item->ItemName,ItemName);
	strcpy(item->ItemValue,ItemValue);

	if(head == NULL)
	{
		item->next = NULL;
		return item;
	}
	else
	{
		struct Item *buffer = head;
		while(buffer->next != NULL && strcmp(buffer->CategoryName,CategoryName) != 0)
			buffer = buffer->next;

		item->next = buffer->next;
		buffer->next = item;
		return head;
	}
}

void DestroyAll(struct Item *head)
{
	struct Item *next;
	while(head)
	{
		next = head->next;
		free(head);
		head = next;
	}
}

char *GetItemValue(struct Item *head,char CategoryName[MAX_LINE],char ItemName[MAX_LINE])
{
	while(head)
	{
		if(strcmp(head->CategoryName,CategoryName) == 0 && strcmp(head->ItemName,ItemName) == 0)
			return head->ItemValue;
		head = head->next;
	}
	return NULL;
}

bool IsItem(struct Item *head,char CategoryName[MAX_LINE],char ItemName[MAX_LINE])
{
	return GetItemValue(head, CategoryName, ItemName) != NULL;
}

bool SetItemValue(struct Item *head,char CategoryName[MAX_LINE],char ItemName[MAX_LINE],char ItemValue[MAX_LINE])
{
	while(head)
	{
		if(strcmp(head->CategoryName,CategoryName) == 0 && strcmp(head->ItemName,ItemName) == 0)
		{
			strcpy(head->ItemValue, ItemValue);
			return true;
		}
		head = head->next;
	}
	return false;
}

struct Item *IncItemValue(struct Item *head,char CategoryName[MAX_LINE],char ItemName[MAX_LINE])
{
	if(IsItem(head, CategoryName, ItemName))
	{
		char buffer[MAX_LINE] = {0};
		char *lpValue = GetItemValue(head, CategoryName, ItemName);
		snprintf(buffer, MAX_LINE, "%d",atoi(lpValue)+1);
		SetItemValue(head, CategoryName, ItemName, buffer);
	}
	else
	{
		head = CreateItem(head, CategoryName, ItemName, NUMBER_ONE);
	}
	return head;
}

bool IsUniq(struct Item *head,char ItemName[MAX_LINE])
{
	while(head)
	{
		if(strcmp(head->ItemName,ItemName) == 0)
			return false;
		head = head->next;
	}
	return true;
}

char *GetNextCategory(struct Item *head,char PrevCategory[MAX_LINE])
{
	bool flagNext = false;
	while(head)
	{
		if(PrevCategory == NULL || (strcmp(head->CategoryName, PrevCategory) != 0 && flagNext))
			return head->CategoryName;

		if(strcmp(head->CategoryName,PrevCategory) == 0 && flagNext == false)
			flagNext = true;

		head = head->next;
	}
	return NULL;
}

bool SaveToIni(char filename[MAX_LINE],struct Item *head)
{
	char CategoryName[MAX_LINE] = {0};

	FILE *f = fopen(filename,"w");
	if(f)
	{
		while(head)
		{
			if(strcmp(CategoryName,head->CategoryName))
			{
				memset(CategoryName, 0, sizeof(CategoryName));
				strcpy(CategoryName, head->CategoryName);
				fprintf(f, "[%s]\n", CategoryName);
			}
			fprintf(f, "%s=%s\n", head->ItemName,head->ItemValue);
			head = head->next;
		}

		fclose(f);
		return true;
	}
	return false;
}

struct Item *LoadFromIni(char filename[MAX_LINE])
{
	char buffer[2*MAX_LINE+1];
	char CategoryName[MAX_LINE];
	FILE *f = fopen(filename,"r");
	if(f)
	{
		struct Item *head = NULL;

		while(!feof(f))
		{
			memset(buffer, 0, sizeof(buffer));
			fgets(buffer, sizeof(buffer), f);

			if(buffer[0] == '#')
				continue;
			else if(buffer[0] == '[')
			{
				memset(CategoryName, 0, sizeof(CategoryName));
				strncpy(CategoryName,&buffer[1],strlen(&buffer[1])-2);
				continue;
			}
			else if(strchr(buffer,'='))
			{
				char *lpItemValue = strchr(buffer,'=');
				*(lpItemValue++) = 0;
				lpItemValue[strlen(lpItemValue)-1] = 0;
				struct Item *temp = CreateItem(head, CategoryName, buffer, lpItemValue);
				if(temp)
					head = temp;
				else
				{
					DestroyAll(head);
					return NULL;
				}
			}
		}

		fclose(f);

		return head;
	}
	return NULL;
}