#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <locale.h>
#include <wchar.h>
#include <wctype.h>
#include "libstemmer.h"

#define BASENAME "./talkbase.ini"
#define BASECFG  "config"