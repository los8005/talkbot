#ifndef TEXT
#define TEXT

#include "headers.h"

#define MAX_TEXT 8*1024
#define LANG_TO_PARSE "russian"
#define CODEPAGE "UTF_8"
#define SEPARATOR " !?.,-+-*/()@#$%^&[]=01234567890~'\"\t\n"

int ParseText(char SourceText[MAX_TEXT],char Result[MAX_TEXT]);

#endif