#include "baies.h"

/*
Dc — количество документов в обучающей выборке принадлежащих классу c;
D — общее количество документов в обучающей выборке;
|V| — количество уникальных слов во всех документах обучающей выборки;
Lc — суммарное количество слов в документах класса c в обучающей выборке;
Wic — сколько раз i-ое слово встречалось в документах класса c в обучающей выборке;
Q – множество слов классифицируемого документа (включая повторы).
*/

bool Train(char ClassName[MAX_LINE],char TrainData[MAX_TEXT])
{
	char ParseData[MAX_TEXT] = {0};

	struct Item *head = LoadFromIni(BASENAME);
	struct Item *tmp;

	tmp = IncItemValue(head, BASECFG, DOCS_PER_TOTAL);
	if(tmp == NULL)
	{
		DestroyAll(head);
		return false;
	}
	else
		head = tmp;

	tmp = IncItemValue(head, ClassName, DOCS_PER_CLASS);
	if(tmp == NULL)
	{
		DestroyAll(head);
		return false;
	}
	else
		head = tmp;

	int count = ParseText(TrainData, ParseData);
	if(count == -1)
	{
		DestroyAll(head);
		return false;
	}

	char *lpWord = ParseData;

	for(int i = 0;i<count;i++,lpWord += (strlen(lpWord)+1))
	{
		if(IsUniq(head, lpWord))
		{
			tmp = IncItemValue(head, BASECFG, UNIQ_WORD_PER_TOTAL);
			if(tmp == NULL)
			{
				DestroyAll(head);
				return false;
			}
			else
				head = tmp;
		}

		tmp = IncItemValue(head, ClassName, WORD_PER_CLASS);
		if(tmp == NULL)
		{
			DestroyAll(head);
			return false;
		}
		else
			head = tmp;

		tmp = IncItemValue(head, ClassName, lpWord);
		if(tmp == NULL)
		{
			DestroyAll(head);
			return false;
		}
		else
			head = tmp;
	}

	bool ret = SaveToIni(BASENAME, head);

	DestroyAll(head);

	return ret;
}

bool AnalyzeReplica(char AnalyzeData[MAX_TEXT],char ClassName[MAX_LINE])
{
	char PClassName[MAX_LINE];
	double PMax;
	bool PFirs = true;

	char ParseData[MAX_TEXT] = {0};

	struct Item *head = LoadFromIni(BASENAME);

	char *cfgval;

	int Dc;
	if((cfgval = GetItemValue(head, BASECFG, DOCS_PER_TOTAL)) == NULL)
	{
		DestroyAll(head);
		return false;
	}
	int D = atoi(cfgval);
	if((cfgval = GetItemValue(head, BASECFG, UNIQ_WORD_PER_TOTAL)) == NULL)
	{
		DestroyAll(head);
		return false;
	}
	int V = atoi(cfgval);
	int Lc;
	int Wic;

	double T;

	int count = ParseText(AnalyzeData, ParseData);
	if(count == -1)
	{
		DestroyAll(head);
		return false;
	}

	char *Category = GetNextCategory(head, BASECFG);
	if(Category == NULL)
	{
		DestroyAll(head);
		return false;
	}

	do
	{
		if((cfgval = GetItemValue(head, Category, DOCS_PER_CLASS)) == NULL)
		{
			DestroyAll(head);
			return false;
		}
		Dc = atoi(cfgval);
		if((cfgval = GetItemValue(head, Category, WORD_PER_CLASS)) == NULL)
		{
			DestroyAll(head);
			return false;
		}
		Lc = atoi(cfgval);

		T = log((double)Dc / (double)D);

		char *lpWord = ParseData;
		
		for(int i = 0;i<count;i++,lpWord += (strlen(lpWord)+1))
		{
			if(IsItem(head, Category, lpWord))
			{
				if((cfgval = GetItemValue(head, Category, lpWord)) == NULL)
				{
					DestroyAll(head);
					return false;
				}
				Wic = atoi(cfgval);
			}
			else
				Wic = 0;

			T += log((double)(Wic+1) / (double)(V + Lc));
		}

		if(PFirs || T>PMax)
		{
			PMax = T;
			strcpy(PClassName,Category);
			PFirs = false;
		}
	}while((Category = GetNextCategory(head, Category)) != NULL);

	DestroyAll(head);

	strcpy(ClassName,PClassName);

	return true;
}