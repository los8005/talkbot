#include "headers.h"
#include "dataio.h"
#include "text.h"
#include "baies.h"

int main(int argc,char *argv[])
{
	if(argc < 2)
	{
		printf("Use -help\n");
		return EXIT_FAILURE;
	}
	else
	{
		if(strcmp(argv[1],"-help") == 0)
		{
			printf("-traine class \"text\" - for training\n"
					"-recognize \"text\" - for recognizing\n"
					"-help - for this help\n");
		}
		else if(strcmp(argv[1],"-traine") == 0)
		{
			if(Train(argv[2], argv[3]) == false)
				printf("Error training!\n");
		}
		else if(strcmp(argv[1], "-recognize") == 0)
		{
			char buffer[MAX_LINE] = {0};
			if(AnalyzeReplica(argv[2], buffer) == false)
				printf("Error analyzing!\n");
			printf("%s\n",buffer);
		}
		else
		{
			printf("Use -help\n");		
		}
	}
	
	return EXIT_SUCCESS;
}