#ifndef DATAIO
#define DATAIO

#include "headers.h"

#define NUMBER_ONE "1"
#define MAX_LINE 256

struct Item
{
	char CategoryName[MAX_LINE];
	char ItemName[MAX_LINE];
	char ItemValue[MAX_LINE];
	struct Item *next;
};

struct Item *CreateItem(struct Item *head,char CategoryName[MAX_LINE],char ItemName[MAX_LINE],char ItemValue[MAX_LINE]);
void DestroyAll(struct Item *head);
char *GetItemValue(struct Item *head,char CategoryName[MAX_LINE],char ItemName[MAX_LINE]);
bool IsItem(struct Item *head,char CategoryName[MAX_LINE],char ItemName[MAX_LINE]);
bool SetItemValue(struct Item *head,char CategoryName[MAX_LINE],char ItemName[MAX_LINE],char ItemValue[MAX_LINE]);
bool SaveToIni(char filename[MAX_LINE],struct Item *head);
struct Item *LoadFromIni(char filename[MAX_LINE]);
struct Item *IncItemValue(struct Item *head,char CategoryName[MAX_LINE],char ItemName[MAX_LINE]);
bool IsUniq(struct Item *head,char ItemName[MAX_LINE]);
char *GetNextCategory(struct Item *head,char PrevCategory[MAX_LINE]);

#endif